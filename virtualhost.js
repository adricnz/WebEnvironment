http = require('http');

server = http.createServer(function(request, response) {
    switch(request.headers.host) {
        case 'example.com': response.write('<h1>Welcome to example.com</h1>'); break;
        case 'not.example.com': response.write('<h1>This is not example.com</h1>'); break;
        default: 
            response.statusCode = 404;
            response.write('<p>We do not serve the host: <b>' + request.headers.host + '</b>.</p>');
    }
    response.end();
});
server.listen(80);